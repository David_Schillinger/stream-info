var rewire = require('rewire')
var broadcastersGet = rewire('../modules/broadcastersGET.js')
var broadcastersPostPut = rewire('../modules/broadcastersPOSTPUT.js')
var broadcastersDelete = rewire('../modules/broadcastersDELETE.js')

describe('POST/PUT/DELETE/GET Tests', () => {

	it('should add a broadcaster object to the database', (done) => {
		broadcastersPostPut.add({ scheme: 'Basic', credentials: 'ZGF2aWRzOnNjaGlsbGlk', basic: { username: 'davids', password: 'schillid' } }, { user: 'lolgeranimo', summoner: 'Geranihohoho', region: 'na' },(data) => {
			expect(data.message).toEqual("Broadcaster added.")
			expect(data.data.broadcaster).toEqual("lolgeranimo")
			expect(data.data.stream.streamAvailable).toEqual(true)
			done()
		})
	})

	it('should update the object in the database', (done) => {
		broadcastersPostPut.update({ scheme: 'Basic', credentials: 'ZGF2aWRzOnNjaGlsbGlk', basic: { username: 'davids', password: 'schillid' } }, { user: 'lolgeranimo', summoner: 'Geranihohoho', region: 'na' },(data) => {
			expect(data.message).toEqual("Broadcaster updated.")
			expect(data.data.broadcaster).toEqual("lolgeranimo")
			expect(data.data.stream.streamAvailable).toEqual(true)
			done()
		})
	})
	
	it('should get the object from the database', (done) => {
		broadcastersGet.search('application/json', 'lolgeranimo', (data) => {
			expect(data.message).toEqual("Broadcaster found.")
			expect(data.data.broadcaster).toEqual("lolgeranimo")
			expect(data.data.stream.streamAvailable).toEqual(true)
			expect(data.data.game.gameAvailable).toEqual(true)
			expect(data.data.stream.game).toEqual("League of Legends")
			done()
		})
	})
	
	it('should remove the object from the database', (done) => {
		broadcastersDelete.delete({ scheme: 'Basic', credentials: 'ZGF2aWRzOnNjaGlsbGlk', basic: { username: 'davids', password: 'schillid' } }, { user: 'lolgeranimo', summoner: 'Geranihohoho', region: 'na' },(data) => {
			expect(data.status).toEqual("success")
			expect(data.message).toEqual("Broadcaster - lolgeranimo - removed from database.")
			done()
		})
	})

})
