var restify = require('restify')
var server = restify.createServer()

server.use(restify.fullResponse())
server.use(restify.queryParser())
server.use(restify.bodyParser())
server.use(restify.authorizationParser())

var broadcastersPostPut = require('./broadcastersPOSTPUT.js');
var broadcastersGet = require('./broadcastersGET.js');
var broadcastersDelete = require('./broadcastersDELETE.js');

server.get('/broadcasters', (req, res) => {
    console.log('GET from /broadcasters')
    const searchTerm = req.query.broadcaster
    const contentType = req['headers']['content-type']
    console.log('broadcaster='+searchTerm)
    broadcastersGet.search(contentType, searchTerm, (data) => {
        console.log('DATA RETURNED')
        console.log(data)
        res.setHeader('content-type', data.contentType);
        res.send(data.code, data.response);
        res.end();
    })
})

server.post('/broadcasters', (req, res) => {
    console.log('POST to /broadcasters')
    const auth = req.authorization
    const body = req.body
    broadcastersPostPut.add(auth, body, (data) => {
        console.log('DATA RETURNED')
        res.setHeader('content-type', 'application/json');
        res.send(data.code, data.response);
        res.end();
    })
})

server.put('/broadcasters', (req, res) => {
    console.log('PUT to /broadcasters')
    const auth = req.authorization
    console.log(auth)
    const body = req.body
    console.log(body)
    broadcastersPostPut.update(auth, body, (data) => {
        console.log('DATA RETURNED')
        res.setHeader('content-type', 'application/json');
        res.send(data.code, data.response);
        res.end();
    })
})

server.del('/broadcasters', (req, res) => {
    console.log('DELETE from /broadcasters')
    const auth = req.authorization
    const body = req.body
    broadcastersDelete.delete(auth, body, (data) => {
        console.log('DATA RETURNED')
        res.setHeader('content-type', 'application/json');
        res.send(data.code, data.response);
        res.end();
    })
})


var port = process.env.PORT || 8080;
server.listen(port, (err) => {
    if (err) {
        console.error(err);
    } else {
        console.log('App is ready at : ' + port);
    }
})