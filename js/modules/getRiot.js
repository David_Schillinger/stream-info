var request = require('request')
const apiKey = '?api_key=740e162a-3054-4288-9959-9403499738b6'

// Helper function to receive a summoner ID assigned to a summoner name.
// Most of the time we only know the summoner name. Making this an essential request.
function getSummonerID(region, name, callback) {
    var url = 'https://' +region+ '.api.pvp.net/api/lol/' +region+ '/v1.4/summoner/by-name/' +name+apiKey
    console.log('Fetching summoner ID from: '+url)
    request.get(url, (error, response, body) => {
        if(!error && response.statusCode == 200) {
            const noWhiteSpaces = name.replace(/\s/g,'')
            const convertedName = noWhiteSpaces.toLowerCase()
            const parsed = JSON.parse(body)
            const summonerID = parsed[convertedName]["id"]
            console.log('Summoner ID found: '+summonerID)
            callback(summonerID)
        } else { callback('summoner not found') }
    })
}

// The return of this helper function is used to build the url to find a currently running game.
function getPlatformID (region) {
    var platformID = ''
    if (region === 'na') {
        platformID = 'NA1'
    } else if (region === 'br') {
        platformID = 'BR1'
    } else if (region === 'eune') {
        platformID = 'EUN1'
    } else if (region === 'euw') {
        platformID = 'EUW1'
    } else if (region === 'kr') {
        platformID = 'KR'
    } else if (region === 'lan') {
        platformID = 'LA1'
    } else if (region === 'las') {
        platformID = 'LA2'
    } else if (region === 'oce') {
        platformID = 'OC1'
    } else if (region === 'ru') {
        platformID = 'RU'
    } else if (region === 'tr') {
        platformID = 'TR1'
    } else console.log('Unavailable region specified.')
    return platformID
}

// The main function to receive current game data.
exports.getSummonerData = (region, name, callback) => {
    getSummonerID(region, name, (summonerID) => {
        var platformID = getPlatformID(region)
        var url = 'https://' +region+ '.api.pvp.net/observer-mode/rest/consumer/getSpectatorGameInfo/' +platformID+ '/' +summonerID+apiKey
        console.log('Current game data from: '+url)
        request.get(url, (error, response, body) => {
	        if (!error && response.statusCode == 200) {
                const parsed = JSON.parse(body)
                const player = parsed.participants.filter((object) => {
                    return object.summonerId == summonerID
                })
                const gameData = { "gameAvailable": true, "map": parsed.mapId, "gameMode": parsed.gameMode, "gameType": parsed.gameType, "teamId": player[0].teamId, "spell1Id": player[0].spell1Id, "spell2Id": player[0].spell2Id, "championId": player[0].championId, "profileIconId": player[0].profileIconId, "summonerName": player[0].summonerName, "summonerId": player[0].summonerId,"gameLengthSeconds": parsed.gameLength }
                callback(gameData)
	        } else { callback({"gameAvailable" : false }) }
        })
    })
}