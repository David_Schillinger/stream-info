var mongo = require('./mongo.js')
var xml2js = require('xml2js')

function searchBroadcaster(searchTerm, contentType) {
    console.log('Looking up broadcaster.')
    return new Promise((resolve, reject) => {
        console.log('  a')
        mongo.getByBroadcaster(searchTerm, info => {
            var data = info
            if (contentType === 'application/json') {
                console.log('application/json specified. No conversion required.')
                data.contentType = 'application/json'
                return resolve(data)
            } else if (contentType === 'application/xml' ) {
                console.log('application/xml specified. Converting data to XML.')
                data.contentType = 'application/xml'
                const jsonTemp = JSON.stringify(data.response)
                const json = JSON.parse(jsonTemp)
                var builder = new xml2js.Builder()
                var newXML = builder.buildObject(json)
                data.response = newXML
                return resolve(data)
            } else {
                console.log('No content-type specified. Default JSON output.')
                data.contentType = 'application/json'
                return resolve(data)
            }
        })
    })
}

exports.search = (contentType, searchTerm, callback) => {
    var broadcaster
    console.log('---1---')
    searchBroadcaster(searchTerm, contentType)
    .then((data) => {
        broadcaster = data
        console.log('---2---')
    }).then(() => {
        console.log('---3---')
        console.log(broadcaster)
        callback(broadcaster)
    }).catch((data) => {
        console.log('---ERROR---')
        console.log('MAIN CATCH')
        callback(data)
  })
}