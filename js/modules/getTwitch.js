var request = require('request')

// Receives some specific Broadcast data and creates an object to hold it.
exports.getStreamData = (stream, callback) => {
	var url = 'https://api.twitch.tv/kraken/streams/' +stream
	request.get(url, (error, response, body) => {
		if (!error && response.statusCode == 200) {
			const parsed = JSON.parse(body)
			if (parsed.stream === null) {
				callback({ "streamAvailable": false })
			} else {
				var streamData = { "streamAvailable": true, "channel": parsed.stream.channel.display_name, "title": parsed.stream.channel.status, "game": parsed.stream.game, "viewers": parsed.stream.viewers, "onlineSince": parsed.stream.created_at, "resolution": parsed.stream.video_height, "fps": parsed.stream.average_fps, "delay": parsed.stream.delay, "preview": parsed.stream.preview.large, "mature": parsed.stream.channel.mature, "language": parsed.stream.channel.language, "channelCreated": parsed.stream.channel.created_at, "logo": parsed.stream.channel.logo, "offlineBanner": parsed.stream.channel.video_banner, "isTwitchPartner": parsed.stream.channel.partner, "url": parsed.stream.channel.url, "followers": parsed.stream.channel.followers }
				callback(streamData)
			}
		} else { callback({ "streamAvailable": false}) }
	})
}

// The following functions are not used in my API right now but are here for future use.

/*// Receives top [data] Twitch games by viewer count.
exports.getTopGames = (data, callback) => {
	var url = 'https://api.twitch.tv/kraken/games/top?limit=' +data
	request.get(url, function(error, response, body) {
		if (!error && response.statusCode == 200) {
			const parsed = JSON.parse(body)
			callback(parsed)
		}
	})
}

// Receives Streams data specified by game and type of information, always in "channel".
// Pushes the data into an array and sends it back.
exports.getDataByGame = (game, limit, type, callback) => {
	var url = 'https://api.twitch.tv/kraken/streams/?game=' +game+ '&limit=' +limit
	request.get(url, (error, response, body) => {
		if (!error && response.statusCode == 200) {
			const parsed = JSON.parse(body)
			var data = []
			for (var i=  0; i < limit; i++) {
				data.push(parsed.streams[i]["channel"][type])
			}
			callback(data)
		}
	})
}*/